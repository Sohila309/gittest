<?php

class Test_WP_Simple_Plugin extends WP_UnitTestCase {

    public function test_constants () {
        $this->assertSame( 'wp-simple-plugin', WPSP_NAME );

        $url = str_replace( 'tests/phpunit/tests/', '',
                trailingslashit( plugin_dir_url( __FILE__ ) ) );
        $this->assertSame( $url, WPSP_URL );
    }
    
    public function test_wpsp_option () {
        $this->assertTrue(get_option('wpsp_test'));
    }

}
