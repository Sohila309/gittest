<?php

/**
 * Plugin Name: WordPress Simple Plugin
 * Plugin URI: http://www.codetab.org/wordpress-tutorial/
 * Description: Plugin to explain WordPress Plugin Basics
 * Version: 1.0.0 
 * Author: sohila
 * Author URI: http://www.codetab.org/about/
 * License: GPLv2
 */

defined( 'ABSPATH' ) or die( "Access denied !" );

define('WPSP_NAME','wp-simple-plugin');

define( "WPSP_URL", trailingslashit( plugin_dir_url( __FILE__ ) ) );
